class WhollyFullySwift < Formula
    desc "Formatting technology for Swift source code"
      homepage "https://gitlab.com/radloffl/wfs"
      url "https://gitlab.com/radloffl/wfs.git",
          tag:      "0.0.1",
          revision: "9b8b349e0c62e74f2269da8a1e944369fcc6019f"
      license "Apache-2.0"
      head "https://gitlab.com/radloffl/wfs.git", branch: "main"
  
  
    depends_on xcode: ["13.0", :build]
  
    def install
      system "swift", "build", "--disable-sandbox", "--configuration", "release"
      bin.install ".build/release/git-wfs"
    end
  
    test do
      system "git-wfs"
    end
  
  end